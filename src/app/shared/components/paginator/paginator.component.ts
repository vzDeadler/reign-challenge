import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.css']
})
export class PaginatorComponent implements OnInit {

	/** */
	public paginatorIndexes: number[];
	/** */
	public selectedIndex: number;
	/** Paginator's default maximum value. */
	private readonly DEFAULT_LIMIT: number = 9;

  constructor() { 
		this.paginatorIndexes = [];
		this.setDefaultOptions();
	}

  ngOnInit(): void { }

	/** Set the default options for paginator from 1 to 9. */
	private setDefaultOptions(): void {
		for(let i = 1; i <= this.DEFAULT_LIMIT; i ++)
			this.paginatorIndexes.push(i);

		this.selectedIndex = this.paginatorIndexes[0];
	}

	/** */
	public selectIndex(_index: number): void {
		if(_index != this.selectedIndex) /** It prevents calling output innecesarily */
			this.selectedIndex = _index;
	}

	/** */
	public navigate(_direction: string): void {
		const paginatorLength = this.paginatorIndexes.length;
		if(_direction == 'left' && this.selectedIndex > this.paginatorIndexes[0]){
			this.selectedIndex --;
		} else if (_direction == 'right' && this.selectedIndex < this.paginatorIndexes[paginatorLength - 1]){
			this.selectedIndex ++;
		}
	}
}
