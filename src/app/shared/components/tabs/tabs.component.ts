import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.css']
})
export class TabsComponent implements OnInit {

	/** It defines which tab is selected, can be the value of: ['all', 'favs']. */
	public tabSelected: string;
	/** Output for emit the tab selection event. */
	@Output() public tabOutput = new EventEmitter<string>();

  constructor() { 
		this.tabSelected = 'all';
	}

  ngOnInit(): void { }

	/**
	 * Switch between tabs and emit the event to the parent component.
	 * @param _tab 
	 */
	public setTab(_tab: string): void {
		this.tabSelected = _tab;
		this.tabOutput.emit(_tab);
	}

}
