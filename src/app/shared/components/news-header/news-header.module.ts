import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewsHeaderComponent } from './news-header.component';

@NgModule({
  declarations: [
		NewsHeaderComponent
	],
  imports: [
    CommonModule
  ],
	exports: [
		NewsHeaderComponent
	]
})
export class NewsHeaderModule { }
