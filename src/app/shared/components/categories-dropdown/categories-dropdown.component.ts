import { Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { Category } from 'src/app/core/models/Category';
import { Asset } from 'src/app/utils/Asset';

@Component({
  selector: 'app-categories-dropdown',
  templateUrl: './categories-dropdown.component.html',
  styleUrls: ['./categories-dropdown.component.css']
})
export class CategoriesDropdownComponent implements OnInit {

	/** Array of category class for displaying on the dropdown list. */
	public categories: Category[];
	/** It shows/hides the dropdown. */
	public shownDropdown: boolean;
	/** Input for the selected category (filter). */
	@Input() public selectedCategory: Category;
	/** */
	@Output() public categoryOutput = new EventEmitter<Category>();

	/** It closes the dropdown if outside click is detected. */
	@HostListener('document:click', ['$event'])
  clickout(_event: any) {
		if(!this.elementRef.nativeElement.contains(event.target)){
			this.shownDropdown = false;
		}
	}

  constructor(
		private elementRef: ElementRef
	) { 
		this.categories = [];
		this.selectedCategory = null;
		this.shownDropdown = false;
	}

  ngOnInit(): void { 
		this.setDefaultCategories();
	}

	/** Initialize values of categories. */
	private setDefaultCategories(): void {
		this.categories.push(new Category('Angular', Asset.ANGULAR_LOGO, 'angular'));
		this.categories.push(new Category('Reacts', Asset.REACT_LOGO, 'reactjs'));
		this.categories.push(new Category('Vuejs', Asset.VUE_LOGO, 'vuejs'));
	}

	/**
	 * Set value of selectedCategory.
	 * @param _category 
	 */
	public setCategory(_category: Category): void {
		this.selectedCategory = _category;
		this.categoryOutput.emit(this.selectedCategory);
		this.shownDropdown = false;
	}

	/** Shows/hides the dropdown. */
	public toggleDropdown(): void {
		this.shownDropdown = !this.shownDropdown;
	}
}