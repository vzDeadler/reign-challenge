import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoriesDropdownComponent } from './categories-dropdown.component';



@NgModule({
  declarations: [
		CategoriesDropdownComponent
	],
  imports: [
    CommonModule
  ],
	exports: [
		CategoriesDropdownComponent
	],
})
export class CategoriesDropdownModule { }
