import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { News } from 'src/app/core/models/News';

@Component({
  selector: 'app-news-card',
  templateUrl: './news-card.component.html',
  styleUrls: ['./news-card.component.css']
})
export class NewsCardComponent implements OnInit {

	/** */
	@Input() news: News;
	/** */
	@Output() favoriteOutput = new EventEmitter<boolean>();

  constructor() { 
		this.news = null;
	}

  ngOnInit(): void { }

	/** */
	public newsClick(): void {
		window.open(this.news.storyURL, "_blank");
	}

	/**
	 * Emit the event of favorite click to parent component.
	 * @param _event 
	 */
	public toggleFavorite(_event: any): void {
		this.favoriteOutput.emit(!this.news.isFavorite);
	}
}
