import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map } from "rxjs/operators";
import { Path } from 'src/app/utils/Path';
import { Category } from '../models/Category';
import { News } from '../models/News';

@Injectable()
export class NewsService {
	constructor(
		private http$: HttpClient
	) { }
	
	/** */
	public getNewsAPI(_page: number, _category?: Category): Observable<News[]> {

		let _queryParams = new HttpParams();
		_queryParams = _queryParams.append('page', _page.toString());

		if(_category) {
			_queryParams = _queryParams.append('query', _category.value);
		}

		return this.http$.get(
			Path.NEWS_SEARCH,
			{ params: _queryParams }
		)
		.pipe(
			map(
				(newsData: any) => {
					let _news: News[] = [ ];

					if(newsData && newsData['hits'].length > 0){
						newsData['hits'].forEach( (_newsValue: any) => {
							let _newsItem: News = new News();

							if(_newsValue['author'] && _newsValue['story_title'] && _newsValue['story_url'] && _newsValue['created_at']) {
								_newsItem.id = _newsValue['objectID'];
								_newsItem.author = _newsValue['author'];
								_newsItem.title = _newsValue['story_title'];
								_newsItem.storyURL = _newsValue['story_url'];
								_newsItem.date = _newsValue['created_at'];

								_news.push(_newsItem);
							}
						})
					}

					return _news;
				}
			),
			catchError(
				(error) => {
					console.error('ERROR: ', error);
					throw(error);
				}
			)
		)
	}
		
}
