import { Asset } from "src/app/utils/Asset";

export class News {

	/** Class attribute description. */
	private _id: string;
	set id(_id: string) { this._id = _id };
	get id(): string { return this._id };
	
	/** Class attribute description. */
	private _author: string;
	set author(_author: string) { this._author = _author };
	get author(): string { return this._author };

	/** Class attribute description. */
	private _date: Date;
	set date(_date: Date) { this._date = _date };
	get date(): Date { return this._date };

	/** Class attribute description. */
	private _title: string;
	set title(_title: string) { this._title = _title };
	get title(): string { return this._title };

	/** Class attribute description. */
	private _storyURL: string;
	set storyURL(_storyURL: string) { this._storyURL = _storyURL };
	get storyURL(): string { return this._storyURL };

	/** Class attribute description. */
	private _isFavorite: boolean;
	set isFavorite(_isFavorite: boolean) { this._isFavorite = _isFavorite };
	get isFavorite(): boolean { return this._isFavorite };

	constructor ( ) {
		this.isFavorite = false;
	}

	/** */
	public getCreationInfo(): string {
		const _newsDate = new Date(this.date).getTime();
		const _actualDate = new Date().getTime();

		const _differenceDate: number = _actualDate - _newsDate;
		
		let _differenceTime: number = Math.floor(_differenceDate / 1000); /** Get time in seconds. */
		let _timeAgo: string = '';

		if(_differenceTime < 60){
			_timeAgo = `${_differenceTime} second${_differenceTime > 1 ? 's' : ''} ago`;
		}else if((_differenceTime / 60) < 60){
			_differenceTime = Math.floor(_differenceTime / 60);
			_timeAgo = `${_differenceTime} minute${_differenceTime > 1 ? 's' : ''} ago`;
		}else if((_differenceTime / 3600) < 24){
			_differenceTime = Math.floor(_differenceTime / 3600);
			_timeAgo = `${_differenceTime} hour${_differenceTime > 1 ? 's' : ''} ago`;
		}else if((_differenceTime / 86400) < 30){
			_differenceTime = Math.floor(_differenceTime / 86400);
			_timeAgo = `${_differenceTime} day${_differenceTime > 1 ? 's' : ''} ago`;
		}else{
			_differenceTime = Math.floor(_differenceTime / 2592000);
			_timeAgo = `${_differenceTime} month${_differenceTime > 1 ? 's' : ''} ago`;
		}

		return `${_timeAgo} by ${this._author}`;
	};

	/** */
	public getHeartImage(): string {
		if(this._isFavorite){
			return `url('${Asset.FILLED_HEART}')`;
		} else {
			return `url('${Asset.EMPTY_HEART}')`;
		};
	}
}