export class Category {

	/** Class attribute description. */
	private _name: string;
	set name(_name: string) { this._name = _name };
	get name(): string { return this._name };

	/** Class attribute description. */
	private _image: string;
	set image(_image: string) { this._image = _image };
	get image(): string { return this._image };

	/** Class attribute description. */
	private _value: string;
	set value(_value: string) { this._value = _value };
	get value(): string { return this._value };

	constructor(_name: string, _image: string, _value: string) {
		this._name = _name;
		this._image = _image;
		this._value = _value;
	}

	/** Returns formatted image url for background image. */
	public getImageURL(): string {
		const _imageURL: string = `url(${this._image})`;
		return _imageURL;
	}
}