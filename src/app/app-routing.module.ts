import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Navigation } from './utils/Navigation';

const routes: Routes = [
	{
		path: Navigation.HACKER_NEWS,
		loadChildren: () => import('./modules/news/news.module').then(m => m.NewsModule)
	},
	{
		path: '',
		redirectTo: Navigation.HACKER_NEWS,
		pathMatch: 'full'
	}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
