import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { NewsService } from 'src/app/core/http/news.service';
import { Category } from 'src/app/core/models/Category';
import { News } from 'src/app/core/models/News';
import { NewsState } from '../states/news.state';

@Injectable()
export class NewsFacade {

  constructor(
		private newsState: NewsState,
		private newsService: NewsService
	) { }

	//#region State
	/** Retrieve the value of news from state. */
	public getNews$(): Observable<News[]> {
		return this.newsState.getNews$();
	}

	/** Retrieve the value of news from state (as value) */
	public getNewsValue(): News[] {
		return this.newsState.getNewsValue();
	}

	/** Retrieve the value of favorite news from state. */
	public getFavoriteNews$(): Observable<News[]> {
		return this.newsState.getFavoriteNews$();
	}

	/** Set the value of news$. */
	public setNews(_news: News[]): void {
		this.newsState.setNews(_news);
	}
	//#endregion

	//#region Services
	/**
	 * Call the api for the news list.
	 * @param _page 
	 * @param _category 
	 */
	public getNewsAPI(_page: number, _category: Category): void {
		this.newsService.getNewsAPI(_page, _category)
			.subscribe( (news) => {
				const storedIDs = JSON.parse(localStorage.getItem('storedIDs'));

				if(storedIDs)
					news.forEach( (_news) => {
						if(storedIDs.indexOf(_news.id) >= 0)
							_news.isFavorite = true;
					})

				this.setNews(news);
			})
	}
	//#endregion

	//#region Facade
	public resetNewsList(): void {
		this.newsState.resetNews();
	}

	/**
	 * Store favorite news on localStorage 
	 * @param news
	 */
	public storeFavoriteNews(news: News): void {
		const storedNews = JSON.parse(localStorage.getItem('storedNews'));
		const storedIDs = JSON.parse(localStorage.getItem('storedIDs'));

		/** If news already exists on storage, exit. */
		if(storedIDs && storedIDs.indexOf(news.id) >= 0)
			return;

		let newStoredNews: News[];
		let newStoredIDs: string[];
		news.isFavorite = true;
			
		if(storedNews){
			newStoredNews = [...storedNews, news];
			newStoredIDs = [...storedIDs, news.id];
		} else {
			newStoredNews = [news];
			newStoredIDs = [news.id];
		}

		this.newsState.setFavoriteNews(news);
		localStorage.setItem('storedNews', JSON.stringify(newStoredNews));
		localStorage.setItem('storedIDs', JSON.stringify(newStoredIDs));
	};

	/** Retrieve all the favorite news to the state. */
	public retrieveFavoriteNews(): void {
		const storedNews = JSON.parse(localStorage.getItem('storedNews'));

		if(!storedNews)
			return; 

		storedNews.forEach( (news: any) => {
			let _news = new News();
			_news.author = news._author;
			_news.id = news._id;
			_news.date = news._date;
			_news.title = news._title;
			_news.storyURL = news._storyURL;
			_news.isFavorite = news._isFavorite;

			this.newsState.setFavoriteNews(_news);
		})
	};

	/**
	 * Remove favorite news from localStorage.
	 * @param news 
	 * @returns 
	 */
	public removeFavoriteNews(news: News): void {
		let storedNews: News[] = JSON.parse(localStorage.getItem('storedNews'));
		let storedIDs: string[] = JSON.parse(localStorage.getItem('storedIDs'));
		let favoriteNewsValues: News[] = this.newsState.getFavoriteNewsValue();
		const storedNewsIndex: number = storedIDs.indexOf(news.id);

		/** If news doesn't exists on storage, exit. */
		if(storedNewsIndex < 0)
			return;

		storedNews.splice(storedNewsIndex, 1);
		storedIDs.splice(storedNewsIndex, 1);

		localStorage.setItem('storedNews', JSON.stringify(storedNews));
		localStorage.setItem('storedIDs', JSON.stringify(storedIDs));

		favoriteNewsValues.splice(storedNewsIndex, 1);
		this.newsState.updateFavoriteNews(favoriteNewsValues);
		
		news.isFavorite = false;
	};

	/**
	 * Save category on localStorage.
	 * @param _category 
	 */
	public storeCategory(_category: Category): void {
		localStorage.setItem('storedCategory', JSON.stringify(_category));
	};

	/**
	 * Retrieve the selected category from localStorage.
	 * @returns 
	 */
	public retrieveCategory(): Category {
		const storedCategory = JSON.parse(localStorage.getItem('storedCategory'));
		
		/** First time user get into the app, it should return null (no filter selected). */
		if(!storedCategory)
			return null;
		
		let _category = new Category(storedCategory._name, storedCategory._image, storedCategory._value);
		return _category;
	}
	//#endregion
}
