import { Component, HostListener, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Category } from 'src/app/core/models/Category';
import { News } from 'src/app/core/models/News';
import { NewsFacade } from '../facades/news.facade';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

	/** News list shown on the view. */
	public news$: Observable<News[]>;
	/** Favorite news list shown on the view. */
	public favoriteNews$: Observable<News[]>;
	/** Used for switching lists and favs, it can be the value of: ['all', 'favs'] */
	public viewMode: string;
	/** Selected category on the view. */
	public category: Category;
	/** Current list page. */
	private newsPage: number;
	/** Semaphore for calling the next list page, it's used on the infinite loading. */
	private scrollSemaphore: boolean;
	/** Subscription to check everytime news api is called. */
	private pageSubscription$: Subscription;

	/** Listener for infinite scroll. */
	@HostListener('scroll', ['$event'])
	onScroll(event: any) {
		if (event.target.offsetHeight + event.target.scrollTop >= event.target.scrollHeight - 50) {
			if(this.scrollSemaphore){
				this.newsFacade.getNewsAPI(this.newsPage, this.category);
				this.scrollSemaphore = false;
			}
		}
	}

  constructor(
		private newsFacade: NewsFacade
	) { 
		this.newsPage = 0;
		this.viewMode = 'all';
		this.category = null;
		this.scrollSemaphore = true;
	}

  ngOnInit(): void {
		this.news$ = this.newsFacade.getNews$();
		this.favoriteNews$ = this.newsFacade.getFavoriteNews$();
		this.category = this.newsFacade.retrieveCategory();
		this.newsFacade.getNewsAPI(this.newsPage, this.category);
		this.newsFacade.retrieveFavoriteNews();
		this.pageSubscription$ = this.newsFacade.getNews$()
			.subscribe( (news) => {
				// Increment the page for next api call, everytime a news list is called.
				if(news.length > 0){
					this.newsPage ++;
					this.scrollSemaphore = true;

					//Call the api until news reach at least 10 news (In case of discarting garbage data)
					if(this.newsFacade.getNewsValue().length < 10){
						this.newsFacade.getNewsAPI(this.newsPage, this.category);
					}
				}
			})
  }

	ngOnDestroy(): void {
		this.pageSubscription$.unsubscribe();
	}

	//#region Facade
	/**
	 * Change tabs between 'favs' and 'all'.
	 * @param _tab 
	 */
	public switchTab(_tab: string): void {
		this.viewMode = _tab;
	}

	/**
	 * Change category, store in on localStorage and call the api.
	 * @param _category 
	 */
	public switchCategory(_category: Category): void {
		this.newsFacade.resetNewsList();
		this.newsPage = 0;
		this.category = _category;
		this.newsFacade.storeCategory(_category);
		this.newsFacade.getNewsAPI(this.newsPage, this.category);
	}

	/**
	 * Set/Remove favorite news.
	 * @param _value 
	 * @param news 
	 */
	public toggleFavorite(_value: boolean, news: News): void {
		if(_value) {
			this.newsFacade.storeFavoriteNews(news)
		} else {
			this.newsFacade.removeFavoriteNews(news)
		}
	}
	//#endregion
}
