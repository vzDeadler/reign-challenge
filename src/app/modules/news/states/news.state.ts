import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { News } from 'src/app/core/models/News';

@Injectable({
  providedIn: 'root'
})
export class NewsState {

	/** Value of news shown */
	private news$: BehaviorSubject<News[]>;
	/** Value of favorite news */
	private favoriteNews$: BehaviorSubject<News[]>;

  constructor() { 
		this.news$ = new BehaviorSubject([]);
		this.favoriteNews$ = new BehaviorSubject([]);
	};

	/** Retrieve the value of news$ as Observable. */
	public getNews$(): Observable<News[]> {
		return this.news$.asObservable();
	};

	/** Retrieve the value of news$. */
	public getNewsValue(): News[] {
		return this.news$.getValue();
	};

	/** Retrieve the value of favoriteNews$ as Observable. */
	public getFavoriteNews$(): Observable<News[]> {
		return this.favoriteNews$.asObservable();
	};

	/** Retrieve the value of favoriteNews$. */
	public getFavoriteNewsValue(): News[] {
		return this.favoriteNews$.getValue();
	};

	/**
	 * Set the value of news. 
	 * @param _news 
	 */
	public setNews(_news: News[]): void {
		const _newsValue: News[] = this.news$.getValue();
		this.news$.next([..._newsValue, ..._news]);
	};

	/** Set the value of news to the initial state. */
	public resetNews(): void {
		this.news$.next([]);
	};

	/**
	 * Set the value of favoriteNews.
	 * @param _news 
	 */
	public setFavoriteNews(_news: News): void {
		const _favoriteNews: News[] = this.favoriteNews$.getValue();
		this.favoriteNews$.next([..._favoriteNews, _news]);
	};

	/**
	 * Update the value of all favorite news.
	 * @param _news 
	 */
	public updateFavoriteNews(_news: News[]): void {
		const _favoriteNews: News[] = this.favoriteNews$.getValue();
		this.favoriteNews$.next(_news);
	};
}
