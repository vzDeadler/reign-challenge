import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewsRoutingModule } from './news-routing.module';

import { NewsComponent } from './containers/news.component';
import { NewsHeaderModule } from 'src/app/shared/components/news-header/news-header.module';
import { TabsModule } from 'src/app/shared/components/tabs/tabs.module';
import { CategoriesDropdownModule } from 'src/app/shared/components/categories-dropdown/categories-dropdown.module';
import { NewsCardModule } from 'src/app/shared/components/news-card/news-card.module';
import { PaginatorModule } from 'src/app/shared/components/paginator/paginator.module';
import { NewsFacade } from './facades/news.facade';
import { NewsState } from './states/news.state';
import { NewsService } from 'src/app/core/http/news.service';

@NgModule({
  declarations: [
		NewsComponent
	],
  imports: [
    CommonModule,
		NewsRoutingModule,
		NewsHeaderModule,
		TabsModule,
		CategoriesDropdownModule,
		NewsCardModule,
		PaginatorModule
  ],
	providers: [
		NewsFacade,
		NewsState,
		NewsService
	]
})
export class NewsModule { }
