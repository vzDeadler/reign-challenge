export class Asset {

	/** IMAGES */
	/** */
	public static ANGULAR_LOGO: string = './assets/images/angular-logo.png';
	/** */
	public static REACT_LOGO: string = './assets/images/react-logo.png';
	/** */
	public static VUE_LOGO: string = './assets/images/vue-logo.png';

	/** ICONS */
	/** */
	public static EMPTY_HEART: string = './assets/icons/empty-heart.svg';
	/** */
	public static FILLED_HEART: string = './assets/icons/filled-heart.svg';
	/** */
	public static LOADER: string = './assets/icons/loader.svg';
}